drop table if exists student;
drop table if exists file_info;

create table student
(
    id         bigserial primary key, -- идентификатор строки - всегда уникальный
    first_name varchar(20),
    last_name  varchar(20),
    age        integer check ( age > 18 and age < 120),
    email varchar(30),
    password varchar(30)
);

create table file_info (
    id bigserial primary key,
    original_file_name varchar(1000),
    storage_file_name varchar(100),
    size bigint,
    mime_type varchar(50),
    description varchar(1000)
)