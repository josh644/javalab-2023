package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.services.FilesService;
import ru.itis.services.ImagesService;

import java.io.IOException;
import java.util.List;

import static ru.itis.constants.Paths.IMAGES_PATH;

@WebServlet(IMAGES_PATH)
public class ImagesServlet extends HttpServlet {

    private ImagesService imagesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.imagesService = context.getBean(ImagesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> images = imagesService.findAllStorageNamesOfImages();
            request.setAttribute("images", images);
        request.getRequestDispatcher("/jsp/images_page.jsp").forward(request, response);
    }
}
