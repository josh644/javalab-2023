package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import static ru.itis.constants.Paths.*;

@WebServlet(name = "profileServlet", urlPatterns = {COLOR_CHANGE_PATH}, loadOnStartup = 0)
public class ProfileServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String color = request.getParameter("color");
        Cookie colorCookie = new Cookie("color", color);
        colorCookie.setPath("/");
        response.addCookie(colorCookie);
        response.sendRedirect(APPLICATION_PREFIX + PROFILE_PAGE);
    }
}
