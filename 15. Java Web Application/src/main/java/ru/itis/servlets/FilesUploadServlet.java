package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.services.FilesService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.itis.constants.Paths.FILES_UPLOAD_PATH;

@WebServlet(FILES_UPLOAD_PATH)
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String description = (new BufferedReader(
               new InputStreamReader(request.getPart("description").getInputStream())).readLine());

        Part filePart = request.getPart("file");

        FileDto uploadedFileInfo = FileDto.builder()
                .size(filePart.getSize())
                .description(description)
                .mimeType(filePart.getContentType())
                .fileName(filePart.getSubmittedFileName())
                .fileStream(filePart.getInputStream())
                .build();

        filesService.upload(uploadedFileInfo);
    }
}
