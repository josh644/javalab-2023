## JWT

* Некоторая строка, которую может проверить сервер и сделать выводы, только на основе этой строки
* Данная строка подписывается сервером, поэтому он может ей доверять
* Главное преимущество - не нужно идти в базу на каждом запросе, вся информация находится в самом токене

## Структура токена

* `HEADER` - заголовок со служебной информацией о том, каким алгоритмом был подписан токен
* `PAYLOAD` - полезная нагрузка (может содержать email пользователя, кго роль, state и т.д., кроме пароля)
* `VERIFY SIGNATURE` - подпись данного токена, основанная на алгоритме, указанном в `HEADER`

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9 - HEADER
eyJzdWIiOiIxIiwiZW1haWwiOiJzaWRpa292Lm1hcnNlbEBnbWFpbC5jb20iLCJyb2xlIjoiQURNSU4iLCJzdGF0ZSI6IkNPTkZJUk1FRCIsImV4cCI6MTY3OTUxMTYwMH0 - PAYLOAD
2MKKwRtSGap9OwsvnqgKOrc8Aj3zhwMlKKNjjrAGd94 - VERIFY SIGNATURE
```

* Алгоритм формирования токена
```
header = base64(" 
    {
        "alg": "HS256",
        "typ": "JWT"
    }") -> eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9

payload = base64("
    {
        "sub": "1",
        "email": "sidikov.marsel@gmail.com",
        "role": "ADMIN",
        "state": "CONFIRMED",
        "exp": 1679511600
    }") -> eyJzdWIiOiIxIiwiZW1haWwiOiJzaWRpa292Lm1hcnNlbEBnbWFpbC5jb20iLCJyb2xlIjoiQURNSU4iLCJzdGF0ZSI6IkNPTkZJUk1FRCIsImV4cCI6MTY3OTUxMTYwMH0
    
VERIFY SIGNATURE = HMACSHA256(header + "." + payload, secret) -> l1ToVQawcl0XFBcFS61SEJTnYE_rxQWoBONuzNZvrCY

JWT = header.payload.verify_signature 
```

### Общий принцип работы токена

* Клиент посылает запрос на сервер с логином и паролем
* Сервер создает JWT-токен, подписывает его ключом, который известен ТОЛЬКО серверу
* Отдает клиенту
* Каждый раз, когда клиент посылает запрос, сервер верифицирует токен и разрешает доступ на основе `PAYLOAD`

### JWT + REDIS

1. JWT не требует обращения в базу данных, поскольку вся информация хранится в самом токене.
2. Но что делать, если токен украли, или человек вышел из сессии? Этот токен мы должны добавить в "черный список".
3. При этом, каждый запрос теперь нужно проверять, не прикреплен ли к нему токен из черного списка?
4. Получается, если мы все токены черного списка будем хранить в СУБД - это лишняя нагрузка.
5. Решение REDIS - in memory no sql database. MAP в виде отдельного СУБД, гарантирует очень высокую скорость работы.

### Настройка REDIS

```
/etc/redis/redis.conf

bind 0.0.0.0 ::1
port 6379
```

### Команды REDIS

```
flushall

set user marsel

get user

exists user

exists user1

flushall

HSET user name marsel

HSET user age 27

HGET user name

GET user age

HGETALL user

HVALS user

HKEYS user

SADD passwords qwerty007 qwerty008 qwerty009

SMEMBERS passwords

SCARD passwords

LPUSH marsel 27 

RPUSH marsel 29

LPANGE marsel 0 3
```

### Docker

## Команды

* `docker build -t [image-name] .` - создает образ на основе папки с `Dockerfile`
* `docker run [image-name]` - запускает контейнер на основе образа
* `docker images` - показывает все образы
* `docker ps` - показывает все контейнеры
* `docker ps -a` - показывает все остановленные контейнеры
* `docker run —name [container-name] [image-name]` - запускает контейнер с заданным именем
* `docker rm [container-id]` - удаляет контейнер по названию
* `docker ps -a -q` - возвращает только id остановленных контейнеров
* `docker rm $(docker ps -qa)` - удаляет все остановленные контейнеры
* `docker run —name [container-name] -d [image-name]` - запускает контейнер в фоне
* `docker stop [container-name]` - остановка контейнера
* `docker run —name [container-name] -d —rm [image-name]` - запускает контейнер в фоне и удаляет после завершения работы
* `docker run —name [container-name] -d —rm -p [host-port:container-port] [image-name]` - пробрасывает порт
* `docker volume ls` - показывает список volumes
* `docker volume create --name node-app-volume` - создание volume
* `docker run —name [container-name] -v [volume-name]:[path-in-container] -d —rm -p [host-port:container-port] [image-name]` - пробрасывает порт
* `winpty docker exec -it [container-name] bash` - открытие консоли контейнера в Windows

## Работа с контейнером REDIS

* Скачиваем готовый образ с REDIS

```
docker pull redis
```

* Сборка Redis-образа с AOF-функцией

```
docker build -t redis-aof .
```

* Создание `volume`

```
docker volume create --name redis_volume
```

* Запуск redis-контейнера

```
docker run --name redis-aof-container-1 -v redis_volume:/data -p 6380:6380 -d redis-aof
```

## Работа с контейнером POSTGRES

* Скачиваем готовый образ POSTGRES

```
docker pull postgres
```

* Создать volume для POSTGRES

```
docker volume create --name postgres_volume
```

* Запуск контейнера

```
docker run -d --name postgresql-container-1 -p 5431:5432 -e POSTGRES_PASSWORD=qwerty009 -e POSTGRES_DB=app_db -v postgres_volume:/var/lib/postgresql/data postgres
```

## Работа с контейнером со Spring-приложением

```
docker run --name application-1 -e SPRING_DATASOURCE_USERNAME=postgres -e SPRING_DATASOURCE_PASSWORD=qwerty009 -e SPRING_DATASOURCE_URL=jdbc:postgresql://172.20.0.3:5432/app_db -e REDIS_HOST=172.20.0.2 -e REDIS_PORT=6380 -p 80:8080 application-image
```
* Узнать адреса контейнеров
```
docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)
```

## Сборка образа в формате `multistage`

```
DOCKER_BUILDKIT=1 docker build -t application-image .
```