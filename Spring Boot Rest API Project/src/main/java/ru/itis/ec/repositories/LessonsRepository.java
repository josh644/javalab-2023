package ru.itis.ec.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ec.models.Lesson;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    Page<Lesson> findAllByStateOrderById(Pageable pageable, Lesson.State state);
}
