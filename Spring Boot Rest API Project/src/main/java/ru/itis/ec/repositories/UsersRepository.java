package ru.itis.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ec.models.User;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email1);
}
