package ru.itis.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ec.models.Token;

import java.util.Optional;

public interface TokensRepository extends JpaRepository<Token, Long> {

    Optional<Token> findFirstByToken(String token);
}
