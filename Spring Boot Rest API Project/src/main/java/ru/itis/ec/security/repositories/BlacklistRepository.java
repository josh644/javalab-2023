package ru.itis.ec.security.repositories;

public interface BlacklistRepository {
    void save(String token);

    boolean exists(String token);
}
