package ru.itis.ec.validation.validators;

import org.springframework.data.util.ReflectionUtils;
import ru.itis.ec.validation.constraints.NotSameValues;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SameValuesValidator implements ConstraintValidator<NotSameValues, Object> {

    private String[] fieldsNames;

    @Override
    public void initialize(NotSameValues notSameValues) {
        this.fieldsNames = notSameValues.fields();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        List<String> fieldsValues = new ArrayList<>();

        for (String fieldName : fieldsNames) {
            Field field = ReflectionUtils.findRequiredField(o.getClass(), fieldName);
            field.setAccessible(true);
            try {
                fieldsValues.add((String)field.get(o));
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
        }

        return fieldsValues.size() == fieldsValues.stream().distinct().count();
    }
}
