package ru.itis.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.ui.UI;
import ru.itis.validation.PasswordValidator;

public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        UI ui = context.getBean(UI.class);
        ui.start();
    }
}
