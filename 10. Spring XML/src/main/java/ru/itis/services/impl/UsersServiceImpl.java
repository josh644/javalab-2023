package ru.itis.services.impl;

import ru.itis.exceptions.PasswordValidationException;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;
import ru.itis.services.UserService;
import ru.itis.validation.PasswordValidator;

public class UsersServiceImpl implements UserService {

    private final PasswordValidator passwordValidator;

    private final UsersRepository usersRepository;

    public UsersServiceImpl(PasswordValidator passwordValidator, UsersRepository usersRepository) {
        this.passwordValidator = passwordValidator;
        this.usersRepository = usersRepository;
    }

    public boolean signUp(String email, String password) {
        try {
            this.passwordValidator.validate(password);
            User user = User.builder()
                    .email(email)
                    .password(password)
                    .build();

            usersRepository.save(user);

            return true;
        } catch (PasswordValidationException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }
}
