package example01;

import java.lang.reflect.*;

public class Main {

    public static void printInformationAboutClass(Class<?> aClass) {
        System.out.println(aClass.getName()
                + " " + aClass.getTypeName()
                + " " + aClass.getSimpleName()
                + " " + aClass.getCanonicalName()
                + " " + aClass.getPackageName());
    }
    public static void printFieldsOfClass(Class<?> aClass) {

        Field[] fields = aClass.getDeclaredFields();

        for (Field field : fields) {
            System.out.println(Modifier.isPublic(field.getModifiers()) + " " + field.getType() + " " + field.getName());
        }
    }

    public static void printMethodsOfClass(Class<?> aClass) {

        Method[] methods = aClass.getDeclaredMethods();

        for (Method method : methods) {
            System.out.print(method.getReturnType() + " " + method.getName() + "(");
            Parameter[] parameters = method.getParameters();
            for (Parameter parameter : parameters) {
                System.out.print(parameter.getType() + " " + parameter.getName() + " ");
            }
            System.out.println(")");
        }
    }

    public static void printConstructorsOfClass(Class<?> aClass) {

        Constructor<?>[] constructors = aClass.getConstructors();

        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor.getParameterCount());
        }
    }

    public static void main(String[] args) throws Exception {
        PasswordValidator validator = new PasswordValidator(10);

//        Class<?> aClass = validator.getClass();
        Class<?> aClass = Class.forName("example01.PasswordValidator");

        printInformationAboutClass(aClass);
        printFieldsOfClass(aClass);
        printMethodsOfClass(aClass);
        printConstructorsOfClass(aClass);
    }
}
