package framework;

import java.time.LocalDate;

public class Document {

    @DefaultValue(value = "Hello!")
    private String title;

    @DefaultValue(value = "2022-02-02")
    private LocalDate createdAt;

    @DefaultValue(value = "false")
    private boolean isProcessed;

    @Override
    public String toString() {
        return "Document{" +
                "title='" + title + '\'' +
                ", createdAt=" + createdAt +
                ", isProcessed=" + isProcessed +
                '}';
    }
}
