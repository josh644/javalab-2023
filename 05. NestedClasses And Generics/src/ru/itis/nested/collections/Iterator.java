package ru.itis.nested.collections;

public interface Iterator {
    boolean hasNext();

    int next();
}
