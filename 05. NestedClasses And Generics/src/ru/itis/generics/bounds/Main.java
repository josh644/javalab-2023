package ru.itis.generics.bounds;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    // rawType
    // является предком всех genetic-ов, указанных с явным типом
    // получение элементов идет с типом Object
    public static void rawList(List list) {
        Object object = list.get(0);
        list.add(new Scanner(System.in));
    }

    // могу получить Object-ы, могу класть Object-ы, но передавать могу только дженерики с Object
    public static void objectsList(List<Object> list) {
        Object object = list.get(0);
        list.add(new Scanner(System.in));
    }

    // wildcards
    // можно подставить любой тип
    // можно получить object
    // нельзя изменять список
    public static void wildcardList(List<?> list) {
        Object obj = list.get(0);
//        list.add(new Animal());
//        list.add(new Tiger());
//        list.add(new Scanner(System.in));
//        list.add(new Object());
    }

    // UPPER BOUNDS
    // можно передать дженерик с типом потомка (либо исходный тип)
    // можно получить любой объект типа ближайшего предка и выше
    // в такой список ничего нельзя добавлять
    public static void upperBoundsList(List<? extends Dog> dogs) {
        Object object = dogs.get(0);
        Animal animal = dogs.get(0);
        Dog dog = dogs.get(0);
//        dogs.add(new Object());
//        dogs.add(new Dog());
//        dogs.add(new Animal());
//        dogs.add(new GoodBoy());
//        dogs.add(new SmallDog());
//        Cat cat = dogs.get(0);
//        GoodBoy goodBoy = dogs.get(0);
//        SmallDog smallDog = dogs.get(0);
    }

    // LOWER BOUNDS
    // можно передать дженерик с типом предка (либо исходный тип)
    // можно получить только Object, больше ничего нельзя
    // класть можно либо исходный тип, либо потомок

    public static void lowerBoundsList(List<? super GoodBoy> goodBoys) {
//        Animal animal = goodBoys.get(0);
//        Dog dog = goodBoys.get(0);
//        GoodBoy goodBoy = goodBoys.get(0);
//        SmallDog smallDog = goodBoys.get(0);
        Object object = goodBoys.get(0);
//        goodBoys.add(new Object());
//        goodBoys.add(new Animal());
//        goodBoys.add(new Dog());
        goodBoys.add(new GoodBoy());
        goodBoys.add(new SmallDog());
    }

    public static void main(String[] args) {
        Animal animal = new Animal();

        Cat cat = new Cat();

        Tiger tiger = new Tiger();

        Dog dog = new Dog();

        GoodBoy goodBoy = new GoodBoy();

        SmallDog smallDog = new SmallDog();

        List<Animal> animals = new ArrayList<>();
        animals.add(animal);
        animals.add(cat);
        animals.add(tiger);
        animals.add(dog);
        animals.add(goodBoy);


        List<Cat> cats = new ArrayList<>();
        cats.add(cat);
        cats.add(tiger);
//        cats.add(animal);
//        cats.add(dog);

        List<Tiger> tigers = new ArrayList<>();
        tigers.add(tiger);

        List<Dog> dogs = new ArrayList<>();
        dogs.add(dog);

        List<GoodBoy> goodBoys = new ArrayList<>();
        goodBoys.add(goodBoy);

        List<SmallDog> smallDogs = new ArrayList<>();
        smallDogs.add(smallDog);

        rawList(animals);
        rawList(cats);
        rawList(tigers);
        rawList(dogs);
        rawList(goodBoys);

//        Animal animal1 = animals.get(5);

        List<Object> objects = new ArrayList<>();

//        objectList(animals);
//        objectList(tigers);
//        objectList(goodBoys);
        objectsList(objects);

        wildcardList(animals);
        wildcardList(tigers);
        wildcardList(dogs);
        wildcardList(objects);

        upperBoundsList(dogs);
        upperBoundsList(goodBoys);
        upperBoundsList(smallDogs);
//        upperBoundsList(animals);
//        upperBoundsList(cats);

        lowerBoundsList(dogs);
        lowerBoundsList(goodBoys);
//        lowerBoundsList(smallDogs);
        lowerBoundsList(animals);
        lowerBoundsList(objects);
//        lowerBoundsList(cats);


    }
}
