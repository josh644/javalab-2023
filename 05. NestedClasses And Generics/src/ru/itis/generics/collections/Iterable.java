package ru.itis.generics.collections;

public interface Iterable<B> {
    Iterator<B> iterator();
}
