package ru.itis.generics.collections;

public class LinkedList<T> implements List<T> {
    Node<T> first;
    Node<T> last;
    private int count;

    public void add(T  value) {
        Node<T> newNode = new Node<>(value);

        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }

        this.last = newNode;

        count++;
    }

    public T get(int index) {
        Node<T> current = first;
        if (index >= 0 && index < count) {
            for (int i = 0; i < index; i++) {
                current = current.next;
            }

            return current.value;
        } else throw new IndexOutOfBoundsException();
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }


    static class Node<E> {
        final E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

    // inner class
    public class LinkedListIterator implements Iterator<T> {

        private Node<T> current;

        public LinkedListIterator() {
            this.current = first;
        }


        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            T value = current.value;
            this.current = current.next;
            return value;
        }
    }

}
