package ru.itis.generics.collections;

import ru.itis.generics.collections.Iterator;
import ru.itis.generics.collections.LinkedList;
import ru.itis.generics.collections.List;

public class Main {

    public static <T> int compare(T a, T b) {
        return 0;
    }

    public static void main(String[] args) {
        LinkedList<Integer> integerList = new LinkedList<>();
//        integerList.add("Hello!");
        integerList.add(10);
        integerList.add(15);
        integerList.add(30);

        List<String> stringList = new LinkedList<>();
        stringList.add("Hello!");
        stringList.add("Bye");
//        stringList.add(5);

        int value = integerList.get(0);

        Iterator<String> iterator = stringList.iterator();

        System.out.println(value);
    }
}
