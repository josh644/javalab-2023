// class version 62.0 (62)
// access flags 0x21
// signature <T:Ljava/lang/Object;>Ljava/lang/Object;Lru/itis/generics/collections/Iterable;
// declaration: ru/itis/generics/collections/LinkedList<T> implements ru.itis.generics.collections.Iterable
public class ru/itis/generics/collections/LinkedList implements ru/itis/generics/collections/Iterable {

  // compiled from: LinkedList.java
  NESTMEMBER ru/itis/generics/collections/LinkedList$LinkedListIterator
  NESTMEMBER ru/itis/generics/collections/LinkedList$Node
  // access flags 0x8
  static INNERCLASS ru/itis/generics/collections/LinkedList$Node ru/itis/generics/collections/LinkedList Node
  // access flags 0x1
  public INNERCLASS ru/itis/generics/collections/LinkedList$LinkedListIterator ru/itis/generics/collections/LinkedList LinkedListIterator

  // access flags 0x0
  // signature Lru/itis/generics/collections/LinkedList$Node<TT;>;
  // declaration: first extends ru.itis.generics.collections.LinkedList$Node<T>
  Lru/itis/generics/collections/LinkedList$Node; first

  // access flags 0x0
  // signature Lru/itis/generics/collections/LinkedList$Node<TT;>;
  // declaration: last extends ru.itis.generics.collections.LinkedList$Node<T>
  Lru/itis/generics/collections/LinkedList$Node; last

  // access flags 0x2
  private I count

  // access flags 0x1
  public <init>()V
   L0
    LINENUMBER 3 L0
    ALOAD 0
    INVOKESPECIAL java/lang/Object.<init> ()V
    RETURN
   L1
    LOCALVARIABLE this Lru/itis/generics/collections/LinkedList; L0 L1 0
    // signature Lru/itis/generics/collections/LinkedList<TT;>;
    // declaration: this extends ru.itis.generics.collections.LinkedList<T>
    MAXSTACK = 1
    MAXLOCALS = 1

  // access flags 0x1
  // signature (TT;)V
  // declaration: void add(T)
  public add(Ljava/lang/Object;)V
   L0
    LINENUMBER 9 L0
    NEW ru/itis/generics/collections/LinkedList$Node
    DUP
    ALOAD 1
    INVOKESPECIAL ru/itis/generics/collections/LinkedList$Node.<init> (Ljava/lang/Object;)V
    ASTORE 2
   L1
    LINENUMBER 11 L1
    ALOAD 0
    GETFIELD ru/itis/generics/collections/LinkedList.count : I
    IFNE L2
   L3
    LINENUMBER 12 L3
    ALOAD 0
    ALOAD 2
    PUTFIELD ru/itis/generics/collections/LinkedList.first : Lru/itis/generics/collections/LinkedList$Node;
    GOTO L4
   L2
    LINENUMBER 14 L2
   FRAME APPEND [ru/itis/generics/collections/LinkedList$Node]
    ALOAD 0
    GETFIELD ru/itis/generics/collections/LinkedList.last : Lru/itis/generics/collections/LinkedList$Node;
    ALOAD 2
    PUTFIELD ru/itis/generics/collections/LinkedList$Node.next : Lru/itis/generics/collections/LinkedList$Node;
   L4
    LINENUMBER 17 L4
   FRAME SAME
    ALOAD 0
    ALOAD 2
    PUTFIELD ru/itis/generics/collections/LinkedList.last : Lru/itis/generics/collections/LinkedList$Node;
   L5
    LINENUMBER 19 L5
    ALOAD 0
    DUP
    GETFIELD ru/itis/generics/collections/LinkedList.count : I
    ICONST_1
    IADD
    PUTFIELD ru/itis/generics/collections/LinkedList.count : I
   L6
    LINENUMBER 20 L6
    RETURN
   L7
    LOCALVARIABLE this Lru/itis/generics/collections/LinkedList; L0 L7 0
    // signature Lru/itis/generics/collections/LinkedList<TT;>;
    // declaration: this extends ru.itis.generics.collections.LinkedList<T>
    LOCALVARIABLE value Ljava/lang/Object; L0 L7 1
    // signature TT;
    // declaration: value extends T
    LOCALVARIABLE newNode Lru/itis/generics/collections/LinkedList$Node; L1 L7 2
    // signature Lru/itis/generics/collections/LinkedList$Node<TT;>;
    // declaration: newNode extends ru.itis.generics.collections.LinkedList$Node<T>
    MAXSTACK = 3
    MAXLOCALS = 3

  // access flags 0x1
  // signature (I)TT;
  // declaration: T get(int)
  public get(I)Ljava/lang/Object;
   L0
    LINENUMBER 23 L0
    ALOAD 0
    GETFIELD ru/itis/generics/collections/LinkedList.first : Lru/itis/generics/collections/LinkedList$Node;
    ASTORE 2
   L1
    LINENUMBER 24 L1
    ILOAD 1
    IFLT L2
    ILOAD 1
    ALOAD 0
    GETFIELD ru/itis/generics/collections/LinkedList.count : I
    IF_ICMPGE L2
   L3
    LINENUMBER 25 L3
    ICONST_0
    ISTORE 3
   L4
   FRAME APPEND [ru/itis/generics/collections/LinkedList$Node I]
    ILOAD 3
    ILOAD 1
    IF_ICMPGE L5
   L6
    LINENUMBER 26 L6
    ALOAD 2
    GETFIELD ru/itis/generics/collections/LinkedList$Node.next : Lru/itis/generics/collections/LinkedList$Node;
    ASTORE 2
   L7
    LINENUMBER 25 L7
    IINC 3 1
    GOTO L4
   L5
    LINENUMBER 29 L5
   FRAME CHOP 1
    ALOAD 2
    GETFIELD ru/itis/generics/collections/LinkedList$Node.value : Ljava/lang/Object;
    ARETURN
   L2
    LINENUMBER 30 L2
   FRAME SAME
    NEW java/lang/IndexOutOfBoundsException
    DUP
    INVOKESPECIAL java/lang/IndexOutOfBoundsException.<init> ()V
    ATHROW
   L8
    LOCALVARIABLE i I L4 L5 3
    LOCALVARIABLE this Lru/itis/generics/collections/LinkedList; L0 L8 0
    // signature Lru/itis/generics/collections/LinkedList<TT;>;
    // declaration: this extends ru.itis.generics.collections.LinkedList<T>
    LOCALVARIABLE index I L0 L8 1
    LOCALVARIABLE current Lru/itis/generics/collections/LinkedList$Node; L1 L8 2
    // signature Lru/itis/generics/collections/LinkedList$Node<TT;>;
    // declaration: current extends ru.itis.generics.collections.LinkedList$Node<T>
    MAXSTACK = 2
    MAXLOCALS = 4

  // access flags 0x1
  public iterator()Lru/itis/generics/collections/Iterator;
   L0
    LINENUMBER 35 L0
    NEW ru/itis/generics/collections/LinkedList$LinkedListIterator
    DUP
    ALOAD 0
    INVOKESPECIAL ru/itis/generics/collections/LinkedList$LinkedListIterator.<init> (Lru/itis/generics/collections/LinkedList;)V
    ARETURN
   L1
    LOCALVARIABLE this Lru/itis/generics/collections/LinkedList; L0 L1 0
    // signature Lru/itis/generics/collections/LinkedList<TT;>;
    // declaration: this extends ru.itis.generics.collections.LinkedList<T>
    MAXSTACK = 3
    MAXLOCALS = 1
}
