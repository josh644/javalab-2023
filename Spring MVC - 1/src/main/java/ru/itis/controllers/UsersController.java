package ru.itis.controllers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import ru.itis.services.UsersService;

@RequiredArgsConstructor
public class UsersController implements Controller {

    private final UsersService usersService;
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("usersView");
        modelAndView.addObject("usersModel", usersService.getAllUsers());
        return modelAndView;
    }
}
