package ru.itis;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.itis.model.User;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_0);
        configuration.setDefaultEncoding("UTF-8");
        Template template;

        try {
            configuration.setTemplateLoader(new ClassTemplateLoader(Main.class.getClassLoader(), "\\"));
            template = configuration.getTemplate("web_template.ftlh");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        User user = User.builder()
                .id(1L)
                .age(null)
                .name("Marsel")
                .build();

        List<User> users = new ArrayList<>();

        users.add(User.builder().name("Алишер").age(19).build());
        users.add(User.builder().name("Нурислам").age(19).build());
        users.add(User.builder().name("Тимербулат").age(18).build());
        users.add(User.builder().name("Кирилл").age(19).build());

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("user", user);
        attributes.put("users", users);

        try (FileWriter writer = new FileWriter("index.html")){
            template.process(attributes, writer);
        } catch (IOException | TemplateException e) {
            throw new IllegalArgumentException(e);
        }
    }
}