function searchUsers(query) {
    return fetch('/users/search?query=' + query)
        .then((response) => {
            return response.json()
        }).then((users) => {
            fillTable(users)
        })
}

function fillTable(users) {
    let table = document.getElementById("usersTable");

    table.innerHTML = '    <tr>\n' +
        '        <th>id</th>\n' +
        '        <th>First Name</th>\n' +
        '        <th>Last Name</th>\n' +
        '        <th>Email</th>\n' +
        '    </tr>';

    for (let i = 0; i < users.length; i++) {
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let firstNameCell = row.insertCell(1);
        let lastNameCell = row.insertCell(2);
        let emailCell = row.insertCell(3)

        idCell.innerHTML = users[i].id;
        firstNameCell.innerHTML = users[i].firstName;
        lastNameCell.innerHTML = users[i].lastName;
        emailCell.innerHTML = users[i].email;
    }
}

function addUser(firstName, lastName, email, password) {
    let body = {
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "password": password
    };

    fetch('/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => response.json())
        .then((users) => fillTable(users))
        .catch((error) => {
            alert(error)
        })
}

function revolveColor() {
    return document.cookie
        .split('; ')
        .find((row) => row.startsWith("color="))?.split('=')[1];
}

function addStyle() {
    let color = revolveColor();
    let css = 'h1 { color:' + color + '; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    head.appendChild(style);

    style.appendChild(document.createTextNode(css))
}