package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.itis.exceptions.PasswordValidationException;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;
import ru.itis.services.UsersService;
import ru.itis.validation.PasswordValidator;

@RequiredArgsConstructor
@Service
public class UsersServiceImpl implements UsersService {

    private final PasswordValidator passwordByLengthValidator;

    private final UsersRepository usersRepository;

    public boolean signUp(String email, String password) {
        try {
            this.passwordByLengthValidator.validate(password);
            User user = User.builder()
                    .email(email)
                    .password(password)
                    .build();

            usersRepository.save(user);

            return true;
        } catch (PasswordValidationException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }
}
