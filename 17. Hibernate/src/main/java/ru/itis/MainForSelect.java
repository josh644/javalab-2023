package ru.itis;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.itis.models.Driver;

public class MainForSelect {
    public static void main(String[] args) {
        Configuration hibernateConfiguration = new Configuration();
        hibernateConfiguration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = hibernateConfiguration.buildSessionFactory();

        Session session = sessionFactory.openSession();

//        Driver driver = session.get(Driver.class, 1L);

        Query<Driver> driverQuery =  session.createQuery("from Driver driver where driver.id = ?1", Driver.class)
                .setParameter(1, 1L);

        Driver driver = driverQuery.getSingleResult();

        System.out.println(driver.getId());
        System.out.println(driver.getCars());

        session.close();
    }
}
