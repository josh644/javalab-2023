package ru.itis;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ru.itis.models.Car;
import ru.itis.models.Driver;

import java.util.ArrayList;
import java.util.HashSet;

public class MainForSave {
    public static void main(String[] args) {
        Configuration hibernateConfiguration = new Configuration();
        hibernateConfiguration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = hibernateConfiguration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        Driver driver = Driver.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .experience(10)
                .cars(new ArrayList<>())
                .build();

        Car reno = Car.builder()
                .color("Черный")
                .model("Renault")
                .mileage(100)
                .build();

        Car audi = Car.builder()
                .color("Красный")
                .model("Audi")
                .mileage(0)
                .build();

        session.persist(audi);
        session.persist(reno);

        driver.getCars().add(reno);
        driver.getCars().add(audi);

        session.persist(driver);

        transaction.commit();

        session.close();


    }
}