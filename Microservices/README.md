## Домашнее задание (для тех, кто на курсе по выбору и на практике)

* Реализовать три приложения:
	* Сервис, предоставляющий информацию об отелях в каком-либо городе
	* Сервис, предоставляющий информацию о кинотеатрах в каком-либо городе
	* Сервис-клиент, у которого можно запросить полную информацию о городе (с отелями и кинотеатрами)
	* Сервисы предоставляют информацию клиенту только через api-ключ