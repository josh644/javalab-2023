package ru.itis.ts.client.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.ts.client.dto.TasksPage;

/**
 * 9/30/2023
 * tasks-service-client
 *
 * @author Marsel Sidikov (AIT TR)
 */
@FeignClient(name = "tasks-service", url = "${feign.tasks-service.url}")
public interface TasksService {

    @GetMapping(value = "/api/tasks", consumes = MediaType.APPLICATION_JSON_VALUE)
    TasksPage getTasks(@RequestParam("page") Integer page, @RequestParam("api-key") String apiKey);
}
