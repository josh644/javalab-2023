package ru.itis.ts.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class TasksServiceClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(TasksServiceClientApplication.class, args);
	}

}
