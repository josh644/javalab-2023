package ru.itis.ts.client.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.ts.client.dto.InfoDto;
import ru.itis.ts.client.dto.TasksPage;
import ru.itis.ts.client.service.TasksService;

/**
 * 9/30/2023
 * tasks-service-client
 *
 * @author Marsel Sidikov (AIT TR)
 */
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@RestController
public class InfoController {

    TasksService tasksService;

    @GetMapping("/info")
    public ResponseEntity<InfoDto> getInfo() {
        TasksPage page = tasksService.getTasks(0, "test-api-key");

        return ResponseEntity.ok(InfoDto.builder()
                        .tasksCount(page.getTasks().size())
                .build());
    }
}
