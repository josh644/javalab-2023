package ru.itis.ts.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.ts.controllers.api.TasksApi;
import ru.itis.ts.dto.NewTaskDto;
import ru.itis.ts.dto.TaskDto;
import ru.itis.ts.dto.TasksPage;
import ru.itis.ts.services.TasksService;

/**
 * 9/9/2023
 * TasksService
 *
 * @author Marsel Sidikov (AIT TR)
 */
@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class TasksController implements TasksApi {

    TasksService tasksService;

    @Override
    public ResponseEntity<TasksPage> getTasks(Integer page) {
        return ResponseEntity.ok(tasksService.getTasks(page));
    }

    @Override
    public ResponseEntity<TaskDto> getTask(Long taskId) {
        return ResponseEntity.ok(tasksService.getTask(taskId));
    }

    @Override
    public ResponseEntity<TaskDto> addTask(NewTaskDto newTask) {
        return ResponseEntity
                .status(201)
                .body(tasksService.addTask(newTask));
    }
}
