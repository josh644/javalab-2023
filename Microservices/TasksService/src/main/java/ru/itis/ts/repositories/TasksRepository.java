package ru.itis.ts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ts.models.Task;

public interface TasksRepository extends JpaRepository<Task, Long> {
}
