package ru.itis.ts.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 9/9/2023
 * TasksService
 *
 * @author Marsel Sidikov (AIT TR)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Страница с задачами")
public class TasksPage {

    @Schema(description = "Список задач")
    private List<TaskDto> tasks;

    @Schema(description = "Общее количество страниц с задачами", example = "10")
    private Integer totalPages;
}
