package ru.itis.ts.services.impl;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.ts.dto.NewTaskDto;
import ru.itis.ts.dto.TaskDto;
import ru.itis.ts.dto.TasksPage;
import ru.itis.ts.exceptions.RestException;
import ru.itis.ts.models.Task;
import ru.itis.ts.repositories.TasksRepository;
import ru.itis.ts.services.TasksService;

import java.time.LocalDate;

import static ru.itis.ts.dto.TaskDto.from;
import static ru.itis.ts.util.DateTimeUtil.EUROPEAN_DATE_TIME_FORMATTER;
import static ru.itis.ts.util.PageUtils.DEFAULT_PAGE_SIZE;

/**
 * 9/9/2023
 * TasksService
 *
 * @author Marsel Sidikov (AIT TR)
 */
@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class TasksServiceImpl implements TasksService {

    TasksRepository tasksRepository;

    @Override
    public TasksPage getTasks(Integer page) {
        PageRequest request = PageRequest.of(page, DEFAULT_PAGE_SIZE, Sort.by("id"));

        Page<Task> tasks = tasksRepository.findAll(request);

        return TasksPage.builder()
                .tasks(from(tasks.getContent()))
                .totalPages(tasks.getTotalPages())
                .build();
    }

    @Override
    public TaskDto getTask(Long taskId) {
        Task task = tasksRepository.findById(taskId)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Task with id <" + taskId + "> not found"));

        return from(task);
    }

    @Transactional
    @Override
    public TaskDto addTask(NewTaskDto newTask) {
        Task task = Task.builder()
                .title(newTask.getTitle())
                .description(newTask.getDescription())
                .start(LocalDate.parse(newTask.getStart(), EUROPEAN_DATE_TIME_FORMATTER))
                .finish(LocalDate.parse(newTask.getFinish(), EUROPEAN_DATE_TIME_FORMATTER))
                .build();

        tasksRepository.save(task);

        return from(task);
    }
}
