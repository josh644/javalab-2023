package ru.itis.words.services;

import ru.itis.words.repositories.WordsRepository;

import java.io.FileNotFoundException;
import java.util.List;

public class WordsServiceImpl implements WordsService {

    private final WordsRepository wordsRepository;

    public WordsServiceImpl(WordsRepository wordsRepository) {
        this.wordsRepository = wordsRepository;
    }

    @Override
    public boolean contains(String word) {
        List<String> words = wordsRepository.findAll();
        return words.contains(word);
    }
}
