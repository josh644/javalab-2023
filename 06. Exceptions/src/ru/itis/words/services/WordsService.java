package ru.itis.words.services;

import java.io.FileNotFoundException;

public interface WordsService {
    boolean contains(String word);
}
