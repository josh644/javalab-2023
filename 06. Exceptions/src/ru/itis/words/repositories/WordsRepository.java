package ru.itis.words.repositories;

import java.io.FileNotFoundException;
import java.util.List;

public interface WordsRepository {
    List<String> findAll();
}
